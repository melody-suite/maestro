<?php

namespace Orchestra\Maestro;

use Orchestra\Helpers\Arr\Arr;
use Orchestra\Maestro\Exceptions\CommandNotFoundException;
use Orchestra\Singleton\Contracts\Singleton;
use Orchestra\Singleton\Traits\Singleton as TraitsSingleton;

class CliRunner implements Singleton
{
   use TraitsSingleton;

   public function run(...$args)
   {
      $command = Arr::get($args, "0.1");

      if (empty($command)) {
         throw new CommandNotFoundException("Cannot run empty commands");
      }

      $commands = cliCommands();

      $command = Arr::get($commands, $command);

      if (empty($command)) {
         throw new CommandNotFoundException($command . " not created to run, please run maestro make:command " . $command);
      }

      return $command::command($args);
   }
}
