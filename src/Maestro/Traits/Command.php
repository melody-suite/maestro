<?php

namespace Orchestra\Maestro\Traits;

trait Command
{
   protected $attributes;

   private function __construct($attributes)
   {
      $this->attributes = $attributes;
   }

   public static function command($attributes)
   {
      $attributes = $attributes[0];

      $attributes = array_slice($attributes, 2);

      $instance = new static($attributes);

      return $instance->run();
   }
}
