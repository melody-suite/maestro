<?php

namespace Orchestra\Maestro\Commands;

use Orchestra\Helpers\Arr\Arr;
use Orchestra\Maestro\Contracts\Command;
use Orchestra\Maestro\Exceptions\CommandVarExpectedException;
use Orchestra\Maestro\Traits\Command as TraitsCommand;

class MakeCommand implements Command
{
   use TraitsCommand;

   public function run()
   {
      $name = Arr::get($this->attributes, "0");

      if (empty($name)) {
         throw new CommandVarExpectedException("Command name expected, usage 'maestro make:command ExampleCommand'");
      }

      $stub = file_get_contents(__DIR__ . "/../Stubs/Command.stub");

      $stub = str_replace("{name}", $name, $stub);

      file_put_contents("src/Commands/$name.php", $stub);
   }
}
