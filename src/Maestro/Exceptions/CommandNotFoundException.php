<?php

namespace Orchestra\Maestro\Exceptions;

class CommandNotFoundException extends \Exception
{
}
