<?php

namespace Orchestra\Maestro\Contracts;

interface Command
{
   public function run();
}
